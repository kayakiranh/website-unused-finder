﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using HtmlAgilityPack;


namespace UnusedCss.Controllers
{
    public class HomeController : Controller
    {
        private static List<string> pageList = new List<string>(); //sayfa listesi
        private static List<string> dataList = new List<string>(); //id ve class listesi
        private static List<string> _styleList = new List<string>(); //style link listesi
        private static List<string> _javascriptList = new List<string>(); //javascript link listesi
        private static List<DataType> styleList = new List<DataType>(); //style listesi
        private static List<DataType> javascriptList = new List<DataType>(); //javascript listesi
        private static string siteFile = "";


        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 1- Dosya Oluştur
        /// 2- Anasayfayı ve her sayfanın 3 alt sayfasının tara
        /// 3- Taranan sayfalardaki id ve classları çek
        /// 4- Taranan sayfalardaki javascript/stylesheet linklerini çek
        /// 5- Javascriptleri download-clean-minify işlemlerine sok
        /// 6- Stylesheetleri download-clean-minify işlemlerine sok
        /// 7- Htmlleri download-clean işlemlerine sok
        /// 8- Kullanıcıya ilgili dataları göster
        /// </summary>
        /// <param name="website"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Scan(string website)
        {
            #region 1- Dosya Oluştur

            string _website = website.ToLower().Replace("://","_").Replace(".", "_").Replace("/", "_").Replace(":", "_");
            var folder = Server.MapPath("~/Files/" + _website);         
            if (!Directory.Exists(folder))
            {
                siteFile = folder;
                Directory.CreateDirectory(folder);
            }
            else
            {
                string dt = (DateTime.UtcNow - new DateTime(2000, 1, 1, 0, 0, 0)).TotalSeconds.ToString();
                siteFile = folder + "_" + dt;
                Directory.CreateDirectory(folder + "_" + dt);
            }

            #endregion

            #region 2- Anasayfayı ve her sayfanın 3 alt sayfasının tara

            pageList = PageScanner(website);

            #endregion

            #region 3- Taranan sayfalardaki id ve classları çek

            dataList = DataScanner();

            #endregion

            #region 4- Taranan sayfalardaki javascript linklerini çek

            _javascriptList = JavascriptScanner();
            _styleList = StyleScanner();

            #endregion

            #region 5- Javascriptleri download-clean-minify işlemlerine sok

            foreach (var javascript in _javascriptList)
            {
                javascriptList.Add(new DataType()
                {
                    originalFile_Local = "",
                    originalFile = javascript,
                    originalFileLength = 0,
                    optimizedFile_Local = "",
                    optimizedFile = "",
                    optimizedFileLength = 0,
                    optimizedDiff = 0,
                    minifiziedFile_Local = "",
                    minifiziedFile = "",
                    minifiziedFileLength = 0,
                    minifiziedDiff = 0
                });
            }

            #endregion

            #region 6- Stylesheetleri download-clean-minify işlemlerine sok

            foreach (var javascript in _javascriptList)
            {
                string ss = DownloadFile(javascript,".js");
                //javascriptList.Add(new DataType()
                //{
                //    originalFile_Local = DownloadFile(javascript),
                //    originalFile = javascript,
                //    originalFileLength = ss.Length,
                //    optimizedFile_Local = "",
                //    optimizedFile = "",
                //    optimizedFileLength = 0,
                //    optimizedDiff = 0,
                //    minifiziedFile_Local = "",
                //    minifiziedFile = "",
                //    minifiziedFileLength = 0,
                //    minifiziedDiff = 0
                //});
            }

            #endregion

            #region 7- Htmlleri download-clean işlemlerine sok


            #endregion

            #region 8- Kullanıcıya ilgili dataları göster


            #endregion

            return RedirectToAction("Index", new { id = "1111" });
        }

        #region SCANNER

        public List<string> PageScanner(string website)
        {
            pageList = new List<string>();
            List<string> scan1 = new List<string>();
            List<string> scan2 = new List<string>();
            List<string> scan3 = new List<string>();
            List<string> scan4 = new List<string>();

            try
            {
                HtmlWeb hw = new HtmlWeb();

                #region SCAN 1. TUR | ANASAYFADAN ÇIKAN LİNKLER

                HtmlAgilityPack.HtmlDocument docScan1 = hw.Load(website);

                foreach (HtmlNode link in docScan1.DocumentNode.SelectNodes("//a[@href]")) 
                {
                    string linkHref = link.Attributes["href"].Value;

                    if (linkHref != "/" && linkHref != "#" && linkHref != "" && !linkHref.Contains("javascript") && !linkHref.Contains("void") && linkHref != website)
                    {
                        string finalUrl = MergeLinkAndWebsite(website, linkHref);

                        if (!linkHref.Contains("http") && !linkHref.Contains("https") && !linkHref.StartsWith("#") && !scan1.Contains(finalUrl))
                        {
                            scan1.Add(finalUrl);
                        }
                        else if (linkHref.StartsWith(website) && !scan1.Contains(finalUrl))
                        {
                            scan1.Add(finalUrl);
                        }
                        else { }
                    }
                }

                #endregion

                #region SCAN 2. TUR

                scan2 = scan2.Concat(scan1).ToList();

                foreach (var scan2Link in scan2)
                {
                    HtmlAgilityPack.HtmlDocument docScan2 = hw.Load(scan2Link);

                    foreach (HtmlNode link in docScan2.DocumentNode.SelectNodes("//a[@href]"))
                    {
                            string linkHref = link.Attributes["href"].Value;

                            if (linkHref != "/" && linkHref != "#" && linkHref != "" && !linkHref.Contains("javascript") && !linkHref.Contains("void") && linkHref != website)
                            {
                                string finalUrl = MergeLinkAndWebsite(website, linkHref);

                                if (!linkHref.Contains("http") && !linkHref.Contains("https") && !linkHref.StartsWith("#") && !scan2.Contains(finalUrl))
                                {
                                    scan2.Add(finalUrl);
                                }
                                else if (linkHref.StartsWith(website) && !scan2.Contains(finalUrl))
                                {
                                    scan2.Add(finalUrl);
                                }
                                else { }
                            }
                    }
                }

                #endregion

                #region SCAN 3. TUR

                scan3 = scan3.Concat(scan2).ToList();

                foreach (var scan3Link in scan3)
                {
                    HtmlAgilityPack.HtmlDocument docScan3 = hw.Load(scan3Link);

                    foreach (HtmlNode link in docScan3.DocumentNode.SelectNodes("//a[@href]"))
                    {

                            string linkHref = link.Attributes["href"].Value;

                            if (linkHref != "/" && linkHref != "#" && linkHref != "" && !linkHref.Contains("javascript") && !linkHref.Contains("void") && linkHref != website)
                            {
                                string finalUrl = MergeLinkAndWebsite(website, linkHref);

                                if (!linkHref.Contains("http") && !linkHref.Contains("https") && !linkHref.StartsWith("#") && !scan3.Contains(finalUrl))
                                {
                                    scan3.Add(finalUrl);
                                }
                                else if (linkHref.StartsWith(website) && !scan3.Contains(finalUrl))
                                {
                                    scan3.Add(finalUrl);
                                }
                                else { }
                            }
                    }
                }

                #endregion

                #region SCAN 4. TUR

                scan4 = scan4.Concat(scan3).ToList();

                foreach (var scan4Link in scan4)
                {
                    HtmlAgilityPack.HtmlDocument docScan4 = hw.Load(scan4Link);

                    foreach (HtmlNode link in docScan4.DocumentNode.SelectNodes("//a[@href]"))
                    {

                            string linkHref = link.Attributes["href"].Value;

                            if (linkHref != "/" && linkHref != "#" && linkHref != "" && !linkHref.Contains("javascript") && !linkHref.Contains("void") && linkHref != website)
                            {
                                string finalUrl = MergeLinkAndWebsite(website, linkHref);

                                if (!linkHref.Contains("http") && !linkHref.Contains("https") && !linkHref.StartsWith("#") && !scan4.Contains(finalUrl))
                                {
                                    scan4.Add(finalUrl);
                                }
                                else if (linkHref.StartsWith(website) && !scan4.Contains(finalUrl))
                                {
                                    scan4.Add(finalUrl);
                                }
                                else { }
                            }
                    }
                }

                #endregion

                pageList = pageList.Concat(scan4).ToList();
            }
            catch { }

            return pageList;
        }

        public List<string> DataScanner()
        {
            dataList = new List<string>();

            foreach (var pageLink in pageList)
            {
                try
                {
                    HtmlWeb hw = new HtmlWeb();
                    HtmlAgilityPack.HtmlDocument doc = hw.Load(pageLink);
                    var pageHtml = doc.DocumentNode.InnerHtml;

                    string[] classList = pageHtml.Split(new string[] { "class" }, StringSplitOptions.None);
                    for (int i = 0; i < classList.Length; i++)
                    {
                        dataList.Add(ReadValue(classList[i]));
                    }
                    string[] idList = pageHtml.Split(new string[] { "id" }, StringSplitOptions.None);
                    for (int i = 0; i < idList.Length; i++)
                    {
                        dataList.Add(ReadValue(idList[i]));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }

            return new List<string>();
        }

        public string ReadValue(string txt)
        {
            int firstStringPosition = txt.IndexOf("=\"");
            int secondStringPosition = txt.IndexOf("\"");
            string stringBetweenTwoStrings = txt.Substring(firstStringPosition, secondStringPosition - firstStringPosition + 2);
            return stringBetweenTwoStrings;
        }

        public List<string> StyleScanner()
        {
            _styleList = new List<string>();

            foreach (var pageLink in pageList)
            {
                try
                {
                    HtmlWeb hw = new HtmlWeb();
                    HtmlAgilityPack.HtmlDocument doc = hw.Load(pageLink);

                    foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//link[@href]"))
                    {
                        string linkHref = link.Attributes["href"].Value;

                        if (linkHref.Contains(".css"))
                        {
                            _styleList.Add(linkHref);
                        }
                        
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }

            return _styleList;
        }

        public List<string> JavascriptScanner()
        {
            _javascriptList = new List<string>();

            foreach (var pageLink in pageList)
            {
                try
                {
                    HtmlWeb hw = new HtmlWeb();
                    HtmlAgilityPack.HtmlDocument doc = hw.Load(pageLink);

                    foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//link[@href]"))
                    {
                        string linkHref = link.Attributes["href"].Value;

                        if (linkHref.Contains(".js"))
                        {
                            _javascriptList.Add(linkHref);
                        }

                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw;
                }
            }

            return _javascriptList;
        }

        #endregion

        #region DOWNLOAD - CLEAN - COMPARE - UPLOAD

        //dosya indirme - download from url
        public string DownloadFile(string fileUrl, string filePath)
        {
            string text;
            using (var client = new WebClient())
            {
                text = client.DownloadString(fileUrl);
            }

            System.IO.FileInfo file = new System.IO.FileInfo(siteFile);
            System.IO.File.WriteAllText(file.FullName, text);


            string path = filePath + @"\Example.txt";

            if (!System.IO.File.Exists(path))
            {
                System.IO.File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("The very first line!");
                }

            }
            else
            {
                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("The next line!");
                }
            }

            return "";
        }

        //styledan kullanılmayanları sil
        public string CleanStyle(string localUrl)
        {
            return "";
        }

        //javascriptten kullanılmayanları sil
        public string CleanJavascript(string localUrl)
        {
            return "";
        }

        //kullanılmayanların silinmiş olduğu styleları minify et
        public string MinifyStyle(string cleanedStyle)
        {
            return "";
        }

        //kullanılmayanların silinmiş olduğu javascriptleri minify et
        public string MinifyJavascript(string cleanedJavascript)
        {
            return "";
        }

        //dosya boyutlarını karşılaştır
        public string CompareFile(string cleanFile, string originalFile)
        {
            return "";
        }

        //link birleştirme ve temizleme
        public string MergeLinkAndWebsite(string website, string link)
        {
            var u = website + link;
            string[] partOfUrl = u.Split('/').Distinct().ToArray();
            string finalUrl = "";
            for (int i = 0; i < partOfUrl.Length; i++)
            {
                if (i == 0)
                {
                    finalUrl += partOfUrl[i] + "//";
                }
                else
                {
                    if (partOfUrl[i] != "")
                    {
                        finalUrl += partOfUrl[i] + "/";
                    }                
                }
            }

            finalUrl = finalUrl.Remove(finalUrl.Length - 1);

            return finalUrl;
        }

        #endregion

        #region MODEL

        public class OptimizedData
        {
            public List<DataType> pages { get; set; }
            public List<DataType> jsFiles { get; set; }
            public List<DataType> cssFiles { get; set; }
        }

        public class DataType
        {
            public string originalFile_Local { get; set; } = "";
            public string originalFile { get; set; } = "";
            public int originalFileLength { get; set; } = 0;
            public string optimizedFile_Local { get; set; } = "";
            public string optimizedFile { get; set; } = "";
            public int optimizedFileLength { get; set; } = 0;
            public int optimizedDiff { get; set; } = 0;
            public string minifiziedFile_Local { get; set; } = "";
            public string minifiziedFile { get; set; } = "";
            public int minifiziedFileLength { get; set; } = 0;
            public int minifiziedDiff { get; set; } = 0;
        }

        #endregion

    }
}